import face_alignment
from skimage import io
import glob
import cv2
import collections
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd



image_height = 48
image_width = 48
pred_tag = collections.namedtuple('Prediction_Tag', ['slice', 'color'])
pred_tags = {"jawline": pred_tag(slice(0, 17), (1, 1, 1, 1)),
             "eyebrow1": pred_tag(slice(17, 22), (1, 1, 1, 1)),
             "eyebrow2": pred_tag(slice(22, 27), (1, 1, 1, 1)),
             "nose": pred_tag(slice(27, 36), (1, 1, 1, 1)),
             "eye1": pred_tag(slice(36, 42), (1, 1, 1, 1)),
             "eye2": pred_tag(slice(42, 48), (1, 1, 1, 1)),
             "mouth": pred_tag(slice(48, 68), (1, 1, 1, 1))}

tags = ["jawline", "eyebrow1", "eyebrow2", "nose", "eye1", "eye2", "mouth"]

image_folder_path = "/mnt/storageDump/Training_clean/"
image_files = glob.glob(image_folder_path + "*.jpg")
device = 'cpu'

number_of_faces = 0

print("importing csv file")
data = pd.read_csv('fer2013.csv')
samples = data['pixels'].values
labels = data['emotion'].values
print(str(len(labels)))

f = open("FER.txt", "w")
for i in range(0, len(samples)):
    image = np.fromstring(samples[i], dtype=np.uint8, sep=" ").reshape((image_height, image_width)) #cv2.imread(image_files[0])

    print(i)
    try:
        fa = face_alignment.FaceAlignment(face_alignment.LandmarksType(3), device=device)
        preds = fa.get_landmarks(image)[0]
    except:
        continue
    # print(preds)
    number_of_faces += 1
    """
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    
    ax.set_xlim([0, 48])
    ax.set_ylim([0, 48])
    
    for tag in tags:
        ax.plot(preds[pred_tags[tag].slice][:, 0], 48 - preds[pred_tags[tag].slice][:, 1], preds[pred_tags[tag].slice][:, 2])
    
    plt.show()
    
    """

    #print("68 " + str(labels[i]))
    f.write("68 " + str(labels[i]) +"\n")
    node_number = 0
    for tag in tags:
        node_tag = str(tags.index(tag))
        for node in range(pred_tags[tag].slice.start, pred_tags[tag].slice.stop):
            line = node_tag + " "
            # print("0 (jaw/eye/nose) 2 (neighbours)")
            if node == pred_tags[tag].slice.start:
                line = line + "1 " + str(node_number + 1)
            elif node == pred_tags[tag].slice.stop - 1:
                line = line + "1 " + str(node_number - 1)
            else:
                line = line + "2 " + str(node_number + 1) + " " + str(node_number - 1)

            line = "{0} {1} {2} {3}".format(line, str(preds[node_number, 0]),
                                            str(preds[node_number, 1]),
                                            str(preds[node_number, 2]))
            #print(line)
            f.write(line +"\n")
            node_number = node_number + 1


f.close()
